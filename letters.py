import numpy as np
import matplotlib.pyplot as plt
from typing import Iterable, Counter
from dataclasses import dataclass
import csv
import datetime
import cartopy
import matplotlib
import cartopy.crs as ccrs
import cartopy.feature as cf
import time
import functools
import itertools
@functools.total_ordering
@dataclass(eq = False)
class Location:
    lat: float
    lng: float
    name: str

    def __hash__(self) -> int:
        return hash((self.lat, self.lng))
    def __eq__(self, other) -> bool:
        return isinstance(other, Location) and hash(self) == hash(other)

    def __lt__(self, other) -> bool:
        self.lat < other.lat or self.lat == other.lat and self.lng < other.lng

@dataclass(order=True)
class Letter:
    src: Location
    dst: Location

    def __hash__(self) -> int:
        return hash((self.src.lat, self.src.lng, self.dst.lat, self.dst.lng))


def groups(letters: Iterable[Letter]) -> Counter[Letter]:
    return Counter(letters)



EUROPE_EPSG_CODE = 3035
def plot(letters: Iterable[Letter]):
    fig = plt.figure(figsize=(12, 12))
    ax = plt.axes(projection=ccrs.PlateCarree())
    groupings = groups(letters)

    destinations = {group.dst: v for group, v in groupings.items()}
    sources = {group.src: v for group, v in groupings.items()}
    count = 0
    for (dst, v) in destinations.items():
        if dst in sources:
            mark, *_ = ax.plot(dst.lng, dst.lat, markersize=(v+sources[dst])*2+7, c='k', marker=f"${chr(ord('a')+count)}$",  transform=ccrs.Geodetic())
            mark.set_label(f"{dst.name}:\n  {v} to \n  {sources[dst]} from")
        else:
            mark, *_ =ax.plot(dst.lng, dst.lat,  markersize=(v*2+7), c='k', transform=ccrs.Geodetic(), marker=f"${chr(ord('a')+count)}$")
            mark.set_label(f"{dst.name}:\n {v} to")

        count += 1
    for (src, v) in  sources.items():
        if src not in destinations:
            mark, *_ =ax.plot(src.lng, src.lat,  markersize=(v*2+7), c='k',  transform=ccrs.Geodetic(), marker=f"${chr(ord('a')+count)}$")
            mark.set_label(f"{src.name}:\n  {v} from")
            count += 1

    plt.legend(loc='upper left')
    names = set()
    for letter in sorted(groupings, key = lambda letter: (letter.src.name, letter.dst.name)):
        src, dst = letter.src, letter.dst
        plt.plot([src.lng, dst.lng], [src.lat, dst.lat], c='k', linestyle='--', transform=ccrs.Geodetic())

    lats = list(itertools.chain((let.src.lat for let in letters), (let.dst.lat for let in letters)))
    lngs = list(itertools.chain((let.src.lng for let in letters), (let.dst.lng for let in letters)))

    ax.set_extent([min(lngs)-1, max(lngs)+1, min(lats)-0.2, max(lats)+3])
    ax.add_feature(cf.BORDERS)
    ax.add_feature(cf.COASTLINE)
    ax.add_feature(cf.LAKES)
    ax.add_feature(cf.OCEAN)
    ax.add_feature(cf.WFSFeature)
    ax.add_feature(cf.RIVERS)

    ax.legend()
    plt.show()


def rounded(s: str) -> float:
    return round(float(s), 2)



def read_letters(path: str) -> Iterable[Letter]:
    with open(path) as f:
        next(f)
        for line in csv.reader(f):
            (_ident, _writer_name, src_location, src_lat, src_lng, _dst_name, dst_location,
             dst_lat, dst_lng, _company, _date, _comments, *_) = line

            try:
                yield Letter(src=Location(lat=rounded(src_lat), lng=rounded(src_lng), name=src_location), dst=Location(lat=rounded(dst_lat), lng=rounded(dst_lng), name=dst_location))
            except ValueError:
                continue


def bottom_left_corner(locs: Iterable[Location]) -> Location:
    return Location(lat = min(loc.lat for loc in locs), lng = min(loc.lng for loc in locs))

def top_right_corner(locs: Iterable[Location]) -> Location:
    return Location(lat = max(loc.lat for loc in locs), lng = max(loc.lng for loc in locs))


if __name__ == '__main__':
    letters = list(read_letters("letters_00.csv"))
    count = groups(letters)
    for (k, v) in sorted(count.items()):
        print(f"{k.src.name} -> {k.dst.name}: \t{v}")
        print(f"\t({k.src.lat} {k.src.lng}) -> ({k.dst.lat} {k.dst.lng})")

    names = set(let.src.name for let in letters)
    plot(letters)
    time.sleep(5)
